package com.entregapr.restaurant.messages.communicator;

public interface DataMessage<T> {

    void onReceiveData(T t);
}
